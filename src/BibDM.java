import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if(liste.size()==0){
            return null;
        }
        Integer mini = liste.get(0);
        for(Integer nombre : liste){
            if(nombre < mini){
                mini = nombre;
            }
        }
        return mini;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        if(liste.isEmpty()){
            return true;
        }
        for(T elem : liste){
            if(valeur.compareTo(elem) >= 0){
                return false;
            }
        }
        return true;
        
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param dicoElem un dictionnaire de liste 
     * @param liste une liste triée
     * @return un. dictionnaire avec ses anciens élements et les éléments de la liste en plus
     */
    public static<T extends Comparable<? super T>> Map<T, Integer> dictionnaireListe(Map<T,Integer> dicoElem, List<T> liste){
        for (T elem : liste){
            if(dicoElem.containsKey(elem)){
                dicoElem.put(elem,dicoElem.get(elem)+1);
            }
            else {
                dicoElem.put(elem,1);
            }
        }
        return dicoElem;
    }

    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        Map<T,Integer> dicoElem = new HashMap<T, Integer>();
        BibDM.dictionnaireListe(dicoElem, liste1);
        BibDM.dictionnaireListe(dicoElem, liste2);
        List<T> liste = new ArrayList<>();
        for(T cle : dicoElem.keySet()){
            if(dicoElem.get(cle)> 1){
                liste.add(cle);
            }
        }
        Collections.sort(liste);
        return liste;
    }

    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> listeMot = new ArrayList<>();
        String mot = "";
        if(texte.length() == 0){
            return listeMot;
        }
        for( int i = 0; i < texte.length(); i++){
            if(texte.charAt(i) == ' ' && !mot.equals("")){
                    listeMot.add(mot);
                    mot="";
            }
            else if (texte.charAt(i) != ' '){
                mot += texte.charAt(i);
            }
        }
        if(!mot.equals("")){
            listeMot.add(mot);
        }
        return listeMot;
    }



    /**
     * Renvoie un Dictionnaire ayant pour clé les mots présent dans la liste, et en valeur le nombre de fois ou ce mot est présent
     * @param liste une liste de mot
     * @return Le Dictionaire avec comme clé un mot et en valeur un Integer
     */

    public static Map<String, Integer> dictionnaire(List<String> liste){
        Map<String, Integer> dicoMot;
        dicoMot = new HashMap<String, Integer>();
        for (String mot : liste){
            if(dicoMot.containsKey(mot)){
                dicoMot.put(mot,dicoMot.get(mot)+1);
            }
            else {
                dicoMot.put(mot,1);
            }
        }
        return dicoMot;
    }

    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        if(texte.length() == 0){
            return null;
        }
        List<String> listeMot = new ArrayList<>();
        listeMot = BibDM.decoupe(texte);                                   //Nous permet d'obtenir une liste de tout les mots présent dans le texte
        Map<String,Integer> dicoMot = new HashMap<String, Integer>();
        dicoMot = BibDM.dictionnaire(listeMot);                            // Nous permet d'obtenir le dictionaire des présent dans la liste et le nombre de répétition du même mots
        String resultat = "";
        int compteur = 0;
        for(String cle : dicoMot.keySet()){     
            if (dicoMot.get(cle)> compteur){                               // Vérifie si la valeur associé à la clé est plus grande que le compteur   
                resultat = cle;
                compteur =  dicoMot.get(cle);
            }
            if (dicoMot.get(cle)== compteur && resultat.compareTo(cle)>0){  //Si la valeur est égale au compteur, alors il compare les 2 cle grâce à la méthode compareTo() présent dans la classe String
                resultat = cle;
            }
        }
        return resultat;
    }
    
    

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parenthesée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        if(chaine.length()==0){
            return true;
        }
        String parenthese = "";
        List<String> listeMot = new ArrayList<>();
        for( int i = 0; i < chaine.length(); i++){
            parenthese += chaine.charAt(i);
            listeMot.add(parenthese);
            parenthese ="";
        }
        Map<String,Integer> dicoMot = new HashMap<String, Integer>();
        dicoMot = BibDM.dictionnaire(listeMot);
                if(listeMot.contains("[") || listeMot.contains("]")){
            return false;
        }
        if(dicoMot.get("(") == dicoMot.get(")") && listeMot.get(0).equals("(") && listeMot.get(listeMot.size()-1).equals(")")){
            return true;
        }
        return false;
    }
    
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parenthesée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        if(chaine.length()==0){
            return true;
        }
        if (BibDM.bienParenthesee(chaine)){
            return true;
        }
        List<String> listeMot = new ArrayList<>();
        String mot = "";
        int compteurP = 0;
        int compteurC = 0;
        for( int i = 0; i < chaine.length(); i++){
            mot += chaine.charAt(i);
            switch(mot){
                case "(" :
                    compteurP+=1;
                break;

                case ")" :
                    compteurP-=1;
                break;

                case "[" :
                    compteurC+=1;
                break;

                case "]" :
                    compteurC-=1;
                break;

                default :
                    compteurC = compteurC;
                break;
            }
            if(compteurP < 0 || compteurC < 0){
                return false;
            }
            listeMot.add(mot);
            mot ="";
        }
                String elemPrec2= "";
        String elemPrec="";
        String maintenant ="";
        int compteur=0;
        for(String elem : listeMot){                //Boucle for qui va permettre de vérifier si il n'y a pas de parenthèses ou de crochets entouré par des parenthèse ou des crochets
            if(compteur == 1){
                elemPrec= maintenant;
            }
            if (compteur == 2){
                elemPrec2 = elemPrec;
                elemPrec = maintenant;
                compteur = 1;
            }
            maintenant = elem;
            compteur+=1;

            if(maintenant.equals("]") && elemPrec.equals(")") && elemPrec2.equals("[")){
                return false;
            }
            if(maintenant.equals("]") && elemPrec.equals("(") && elemPrec2.equals("[")){
                return false;
            }
            if(maintenant.equals(")") && elemPrec.equals("[") && elemPrec2.equals("(")){
                return false;
            }
            if(maintenant.equals(")") && elemPrec.equals("]") && elemPrec2.equals("(")){
                return false;
            }
        }
        Map<String,Integer> dicoMot = new HashMap<String, Integer>();
        dicoMot = BibDM.dictionnaire(listeMot);
        if(dicoMot.get("(") == dicoMot.get(")") && dicoMot.get("[") == dicoMot.get("]") && ((listeMot.get(0).equals("(") && listeMot.get(listeMot.size()-1).equals(")")) || (listeMot.get(0).equals("[") || listeMot.get(listeMot.size()-1).equals("]"))) ){
            return true;
        }
        return false;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if(liste.size() == 0){
            return false;
        }
        int bas = 0;
        int haut = liste.size()-1;
        while(bas<haut){
            int milieu = (bas+haut)/2;
            if(liste.get(milieu)<valeur){
                bas = milieu+1;
            }
            else {
                haut = milieu;
            }
        }
        return bas<liste.size() && valeur == liste.get(bas);
    }

}
